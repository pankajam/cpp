#include <iostream>

using namespace std;

int main() {

	int age = 20;
	int weight = 62;

	cout << age << endl; 
	cout << &age << endl;    // &variable name gives address of variable
	cout << weight << endl; 
	cout << &weight << endl; 

	//begin pointer here
	bool ishuman = true;

	int *ageptr; //pointer 
	bool *ishumanptr;

	ageptr = &age;
	ishumanptr = &ishuman; 

	cout << "age = " << age << " age address = " << ageptr << endl;
	cout << "human = " << ishuman << " ishuman address = "  << ishumanptr << endl;

	cout << "age = " << *ageptr << " age address = " << ageptr << endl;
	cout << "human = " << *ishumanptr << " ishuman address = "  << ishumanptr << endl;

	return 0;
}
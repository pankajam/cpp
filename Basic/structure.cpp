#include <iostream>

using namespace std;

struct student {
	int rollno;
	char sex;
};

int main() {

	student pankaj, chandan;

	pankaj.rollno = 1;
	pankaj.sex = 'm';

	chandan.rollno = 2; // access using varible name;
	chandan.sex = 'm';

	cout << pankaj.rollno << " " << pankaj.sex << endl;

	student *pankajptr;

	pankajptr = &pankaj;

	pankajptr->rollno = 5; // access using pointer
	pankajptr->sex = 'm'; 

	cout << pankajptr->rollno << " " << pankajptr->sex << endl;


	return 0;
}
#include <iostream>
#include <string>

using namespace std;

struct address
{
	int house_no;
	string road_name;
};

struct student
{
	int rollno;
	char sex;
	address adrs;
};

int main() {

	//usinf dot operator
	student pankaj;

	pankaj.rollno = 1;
	pankaj.sex = 'm';
	pankaj.adrs.house_no = 123;
	pankaj.adrs.road_name = "m g road";

	cout << pankaj.rollno << " " << pankaj.sex << " " << pankaj.adrs.house_no << " " << pankaj.adrs.road_name << endl;

	//using arrow operator

	student chandan;

	student *chandanptr;

	chandanptr = &chandan;

	chandanptr->rollno = 10;
	chandanptr->sex = 'm';
	chandanptr->adrs.house_no = 1233;
	chandanptr->adrs.road_name = "g t road";	

	cout << chandanptr->rollno << " " << chandanptr->sex << " " << chandanptr->adrs.house_no << " " << chandanptr->adrs.road_name << endl;
 

	return 0;
}
/*function with same name
	and different parameters
	1. return type same
	2. number of parameter different
*/

#include <iostream>
#include <string>

using namespace std;

void display();
void display(string);

int main (){

	display();
	display("Hello Dear!");

	return 0;
}

void display() {
	cout << "Hello friend!" << endl;
}

void display(string s) {
	cout << s << endl;
}
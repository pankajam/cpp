#include <iostream>

using namespace std;

void show(int arr[], int length) {

	for(int count = 0; count < length; count++){
		cout << arr[count] << endl;
	}
}
int main() {

	int arr[] = {2, 4, 7, 12, 21};
	int length = 5;

	show(arr, length);

	return 0;
}
/* default value function
 return to function
 inline keyword*/ 

#include <iostream>

using namespace std;

void display();
//void sum(int, int);

void sum(int a, int b){
	cout << "sum = " << a+b << endl;
}


int main() {
	display();
	sum(20, 30);

	return 0;
}

void display() {
	cout << "Wellcome to function!" << endl;
}
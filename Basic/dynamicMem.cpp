#include <iostream>

using namespace std;

int main() {
	/*int *ptr;

	ptr = new int;

	*ptr = 10;

	cout << *ptr << endl;

	delete ptr;
*/
	int *pointer = NULL;

	cout << " How many item want you want to entre: ";

	int input;

	cin >> input;

	pointer = new int[input];

	int temp;

	for(int i=0; i<input; i++){
		cout << " Entre " << i+1 << " item: ";
		cin >> temp;

		*(pointer+i) = temp;
	}

	cout << " Your Items are:" << endl;
	for(int i=0; i<input; i++){
		cout << i+1 << " Item is " << *(pointer+i) << endl;
	}

	delete []pointer;

	return 0;
}
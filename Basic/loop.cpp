#include <iostream>

using namespace std;

int main() {
	int count = 1;

	cout << "While loop" << endl;
	// while loop
	while(count <= 5) {
		cout << "count = " << count << endl;
		count++;
	}

	cout << "do While loop" << endl;
	// do while loop
	do {
		cout << "count = " << count << endl;
		count++;
	}while(count <= 10);

	cout << "for loop" << endl;
	// for loop
	for(count=11; count<=15; count++){
		cout << "count = " << count << endl;
	}

	return 0;
}
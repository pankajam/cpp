#include <iostream>

using namespace std;

void show(int *age) {

	cout << *age << endl;

	*age = 50;
	
}

int main() {

	int age = 25;

	show(&age);

	cout << age << endl;

	return 0;
}
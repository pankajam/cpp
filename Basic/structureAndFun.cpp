#include <iostream>

using namespace std;

struct student
{
	int rollno;
	char sex;
	int age;
};

void show(student s);
void display(student *s);

int main()
{
	student pankaj = {1, 'm', 21};

	/*show(pankaj);

	display(&pankaj);*/

	display(&pankaj);

	show(pankaj);

	return 0;
}

void show(student s)
{
	cout << s.rollno << " " << s.sex << " " << s.age << endl;

	//s.rollno = 100;
}

void display(student *s)
{
	cout << s->rollno << " " << s->sex << " " << s->age << endl;
	s->rollno = 100;
}
#include <iostream>
#include <list>
#include <iterator>

using namespace std;


void showlist( list <int> a) {
	list <int> :: iterator it;

	for(it = a.begin(); it !=a.end(); ++it){
		cout << " " << *it;		
	}
	cout << '\n';
}

int main () {
	list <int> list1, list2;

	for(int i=0; i<5; i++) {
		list1.push_back(i*2-1);
		list2.push_back(i*3);
	}

	cout << "LIST 1 is :" << endl;
	showlist(list1);

	cout << "LIST 2 is :" << endl;
	showlist(list2);

	cout << "List 1 front : " << list1.front() << endl;
	cout << "List 2 front : " << list2.front() << endl;

	cout << "List 1 back : " << list1.back() << endl;
	cout << "List 2 back : " << list2.back() << endl;

	list1.push_front(100);
	cout << "After push_front LIST 1 is :" << endl;
	showlist(list1);

	list2.push_back(100);
	showlist(list2);

	cout << "After pop from front : " << endl;
	list1.pop_front();
	showlist(list1);

	cout << "After pop from back : " << endl;
	list1.pop_back();
	showlist(list1);


	return 0;
}
#include <iostream>
#include <algorithm>
#include <numeric>


using namespace std;


void show(int a[]){

	for(int i=0; i<11; i++){
		cout << a[i] << " ";
	}
	cout << endl;

	if(binary_search(a, a+10, 17))
		cout << "FOUND" << endl;
	else
		cout << "NOT FOUND" << endl;
}

int main () {
	int a[] = {0, 1, 5, 8, 9, 6, 7, 3, 4, 2, 0};

	cout << "array before sorting" <<endl;

	show(a);

	sort(a, a+11);

	cout << "array after sorting" <<endl;

	show(a);

	cout << "array reverse" <<endl;

	reverse(a, a+11);

	show(a);

	cout << "array max: " << *max_element(a,a+11) <<endl;
	cout << "array min: " << *min_element(a,a+11) <<endl;
	cout << "array sum: " << accumulate(a,a+11,0) <<endl;

	cout << "count : " << count(a,a+11,0) <<endl;
	cout << "Find : " << find(a,a+11,0) <<endl;

	int *b;
	b = lower_bound(a,a+11,0);
	cout << "lower_bound : " << *b <<endl;

	int *c;
	c = upper_bound(a,a+11,4);
	cout << "upper_bound : " << *c <<endl;




}
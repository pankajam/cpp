#include <iostream>
#include <utility>
#include <string>

using namespace std;

int main () {
	pair <string, int> a1;
	pair <string, int> a2("C++", 1);
	pair <string, int> a3(a2);
	pair <int, int> a4(3, 5);

	cout << "Value of a1 is :" << endl;
	cout << a1.first << " " << a1.second << endl;

	cout << "Value of a2 is :" << endl;
	cout << a2.first << " " << a2.second << endl;

	cout << "Value of a3 is :" << endl;
	cout << a3.first << " " << a3.second << endl;

	cout << "Value of a4 is :" << endl;
	cout << a4.first << " " << a4.second << endl;

	cout << "After swaping :" << endl;
	// swap(a1, a4);  wrong
	swap(a1, a2); 

	cout << "Value of a1 is :" << endl;
	cout << a1.first << " " << a1.second << endl;

	cout << "Value of a2 is :" << endl;
	cout << a2.first << " " << a2.second << endl;

	return 0; 

}
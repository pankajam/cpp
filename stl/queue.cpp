#include <iostream>
#include <queue>
#include <vector>
#include <utility>
#include <list>
#include <iterator>
#include <iterator>
#include <algorithm>

using namespace std;


void showque(queue <int> a) {
	queue <int> i = a;

	while(!a.empty()){
		cout << " " << a.front();
		a.pop();
	}
	cout << '\n';
}

int main () {
	queue <int> a;

	a.push(10);
	a.push(3);

	cout << "queue front is : " << a.front() << '\n';
	cout << "queue back is : " << a.back() << '\n';
	cout << "queue size is : " << a.size() << '\n';
	cout << "queue is empty : " << a.empty() << '\n';


	cout << "queue is :" << '\n';
	showque(a);

	/*
		Priority queue is a queue container under c++
		stl such that first element greater than all
		element in queue.

		priority_queue <int> a; 
	*/

	return 0;
}

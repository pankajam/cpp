#include <iostream>
#include <vector>
using namespace std;

int main() {
	// your code goes here
	vector <int> vec;
	//push_back
	for(int i=0; i<10; i++){
	    vec.push_back(i);
	}
	for(int i=0; i<10; i++){
	    cout << vec[i] << " ";
	}
	cout << endl;
	
	//pop_back
	for(int i=0; i<2; i++){
	    vec.pop_back();
	}
	for(int i=0; i<vec.size(); i++){
	    cout << vec[i] << " ";
	}
	cout << endl;
	
	//insert constant 
	int a = 100;
	
	vector<int> :: iterator it;
	
	it = vec.begin();
	
	vec.insert(it+1, a);
	
	for(int i=0; i<vec.size(); i++){
	    cout << vec[i] << " ";
	}
	cout << endl;
	
	//errase	
	vec.erase(it+4, it+6);
	
	for(int i=0; i<vec.size(); i++){
	    cout << vec[i] << " ";
	}
	cout << endl;
	
	return 0;
}


#include <iostream>
#include <stack>
#include <vector>
#include <iterator>
#include <utility>
#include <list>
#include <queue>
#include <algorithm>

using namespace std;

void showstack (stack <int> a){
	stack <int> i = a;

	while(!a.empty()){
		cout << " " << a.top();
		a.pop();
	}
	cout << '\n';
}
int main () {
	stack <int> a;

	a.push(10);
	a.push(3);
	a.push(4);


	cout << "stack is :" << '\n';
	showstack(a);
}
#include <iostream>
#include <deque>
#include <iterator>
#include <algorithm>
#include <vector>
#include <utility>
#include <string>
#include <list>

using namespace std;


void showdeque (deque <int> a) {
	deque <int> :: iterator it;

	for(it = a.begin(); it != a.end(); ++it) {
		cout << " " << *it;
	}
	cout << '\n';
}
int main () {

	deque <int> a;
	a.push_back(10);
	a.push_back(4);
	a.push_back(10);
	a.push_front(3);

	showdeque(a);
}

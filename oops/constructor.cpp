#include <iostream>
#include <string>

using namespace std;

class Human{
public:
	Human() {
		cout << "Constructor is called when object created" << endl;
	}

};
int main() {
	Human me;
	return 0;
}

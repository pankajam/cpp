#include <iostream>

using namespace std;

class Human {
private:
	int age;
public:
	void displayAge(){
		cout << age << endl;
	}

	void setAge(int value){
		age = value;
	}
};
int main() {
	Human me;
	me.setAge(21);
	me.displayAge();
	
	return 0;
}
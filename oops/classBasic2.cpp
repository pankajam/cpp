#include <iostream>
#include <string>

using namespace std;

class HumanBeing
{
public:
	string name;

	void display(){
		cout << "Hi I'm " << name << endl;
	}
	
};

int main() {

	HumanBeing me;  // this will be static and allocated in stack of memory

	me.name = "Pankaj";

	me.display();

	HumanBeing *pankaj = new HumanBeing();  // this will be dynamic and allocated in heap of memory

	pankaj->name = "Pankaj";

	pankaj->display();

	return 0;
}
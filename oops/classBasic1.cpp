#include <iostream>

using namespace std;

/* this class available to all function
	because it is global
*/

class HumanBeing{
public :									// statement access to other function
	void display() {
		cout << "c++ class" << endl;
	}
};

int main() {

	HumanBeing me;				// object to class

	me.display();				

	return 0;
}